const express = require('express');
const router = express.Router();

const users = [
  {name : "superman", age : 33},
  {name : "catwoman", age : 32},
  {name : "batman", age: 40}
]

router.get('/', function(req, res, next) {
  res.send({
    users : users 
  });
});

router.post('/', function(req,res) {
  users.push(req.body);
  res.send({users: users});
});

//PUT
//how can i update a list of super heroes

router.put('/', function(req,res) {
  // console.log(req.body);
  // users = req.body.users;

  users.length = 0;
  (req.body).users.forEach(user => {
    users.push(user);
  });

  res.send('PUT route');
});

//DELETE
router.delete('/', function(req,res) {

  users.length = 0;
  res.send({users : users});
});

router.delete('/:superheroName', function(req,res) {
  console.log(req.params);
  console.log(req.params.superheroName);

  const foundIndex = users.findIndex(user => user.name == req.params.superheroName);
  if (foundIndex >= 0 ) {
    users.splice(foundIndex,1);
    res.json({users: users})
  }
  else {
    //res.send({resp : "User is not found"})
    res.json({resp : "User is not found"});
  }
  //req.send(req.params);
});

// 1- write an api to fetch one specific user by name - get api
router.get('/:superheroName', function(req,res) {
  const foundIndex = users.findIndex(user => user.name == req.params.superheroName);
  if (foundIndex >= 0) {
    const superhero = users[foundIndex];
    res.json({users: superhero})
  } else {
    res.json({resp: "User is not found"});
  }
})


// 2-
// write an api to edit one specific user by name - put api
router.put('/:superheroName', function(req,res) {
  const foundIndex = users.findIndex(user => user.name == req.params.superheroName);
  if (foundIndex >= 0) {
    users[foundIndex] = req.body;
    res.json({users: users})
  } else {
    res.json({resp: "User is not found"});
  }
})



module.exports = router;
