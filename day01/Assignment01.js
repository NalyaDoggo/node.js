
const students = [{
    name: "superwoman",
    marks: 90
},
{
    name: "flash",
    marks: 70
},
{
    name: "batman",
    marks: 77
},
{
    name: "superman",
    marks: 60
},
{
    name: "arrow",
    marks: 94

}
]

//sort all students by marks from highest to lowest using sort method
students.sort((a,b) => (a.marks < b.marks) ? 1 : -1);

console.log(students);

//Filter and display all students with marks greaer that 80 using filter method
let above80 = students.filter(a => a.marks > 80);

console.log(above80);


//create a new array from the array in which 
//the marks of all  students is 5 more than current marks using map method

let marksPlus5 = students.map(student => {
    const container = {};

    container.name = student.name;
    container.marks = student.marks+5;
    return container;
});

console.log(marksPlus5);

