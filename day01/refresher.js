// let cat = "Hello";

// if (String.length < 10) {
//     console.log(cat);
// }

// for (let i=0; i < 3; i++) {
    
//     for (let j=i; j>0; j--) {
//         console.log(cat + " " + j);
//     }
// }
//while

//do ... while

//foreach

const student = {
    name : "flash",
    studentId : "1234",
    courses : ["JS", "Java", "PHP", "Python"],
    program : {
        name : "IPD",
        duration : "12 months",
        noOfCourses: 10,
        campus : {
            name : "JAC",
            rating : "5/5",
            awesome : "super duper!!!"
        }
    }
}

function adder(x,y) {
    const sum = x+y;
    return sum;
}

const adder2 = function(x,y) {
    const sum = x+y;
    return sum;
}

const adder3 = (x,y) => {
    const sum = x+y;
    return sum;
}

const number = [1,2,3,4,5,6,7,8,9];
number.push(10);

number.forEach(function(number) {
    console.log(number * 2);
})

//lambda expression
number.forEach(number=> console.log(number * 2));

//includes
console.log(number.includes(4));