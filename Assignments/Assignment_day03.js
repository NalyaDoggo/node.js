// 1- write an api to fetch one specific user by name - get api
router.get('/:superheroName', function(req,res) {
  const foundIndex = users.findIndex(user => user.name == req.params.superheroName);
  if (foundIndex >= 0) {
    const superhero = users[foundIndex];
    res.json({users: superhero})
  } else {
    res.json({resp: "User is not found"});
  }
})


// 2-
// write an api to edit one specific user by name - put api
router.put('/:superheroName', function(req,res) {
  const foundIndex = users.findIndex(user => user.name == req.params.superheroName);
  if (foundIndex >= 0) {
    users[foundIndex] = req.body;
    res.json({users: users})
  } else {
    res.json({resp: "User is not found"});
  }
})

// JSON body used in PUT.
//{
//    "name" : "Wolverine",
//    "age" : 35
//}