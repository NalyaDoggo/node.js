var express = require('express');
var router = express.Router();


const orders = [{
  id: 1,
  productName: "Reebok shoes",
  price: 200,
  color: "black",
  quantity: 200
},
{
  id: 2,
  productName: "sports t shirt",
  price: 100,
  color: "blue",
  quantity: 100
}
];


// Create the following routes :

// single resource
// - get api to get one order by id
// - put api to edit one order by id
// - delete apit to delete one order by id
// - post api to create a new order

// All resources
// - get api to fetch all the orders
// - put api to edit all the orders
// - delete api to delete all orders

/* GET listing. */
router.get('/', function(req, res, next) {
  res.send({
    orders : orders
  });
});

/* POST new order */
router.post('/', function(req,res) {
  orders.push(req.body);
  res.send({orders: orders});
});

/* DELETE all orders */
router.delete('/', function(req,res) {
  orders.length = 0;
  res.send({orders : orders});
});

/* PUT edit for all orders */
router.put('/', function(req,res) {
  // console.log(req.body);
  // users = req.body.users;

  orders.length = 0;
  (req.body).orders.forEach(order => {
    orders.push(order);
  });

  res.send('PUT route');
});

/* GET for single order */
router.get("/:identity", function(req,res) {
  const foundIndex = orders.findIndex(order => order.id == req.params.identity);
  if (foundIndex >= 0) {
    const order = orders[foundIndex];
    res.json({orders: order})
  } else {
    res.json({resp: "Order is not found"});
  }
})

/* DELETE for single order */
router.delete('/:identity', function(req,res) {
  const foundIndex = orders.findIndex(order => order.id == req.params.identity);
  if (foundIndex >= 0 ) {
    orders.splice(foundIndex,1);
    res.json({orders: orders})
  }
  else {
    res.json({resp : "Order is not found"});
  }
});

/* PUT for single order */
router.put('/:identity', function(req,res) {
  const foundIndex = orders.findIndex(order => order.id == req.params.identity);
  if (foundIndex >= 0) {
    orders[foundIndex] = req.body;
    res.json({orders: orders})
  } else {
    res.json({resp: "Order is not found"});
  }
})


module.exports = router;
